# gltf Animation
This peace of software was created for the Diplomarbeit from Jörg Eckhold in 2018. 
It is based on https://github.com/filonik/pygltf. It extends the base-software in the usage, to create morph-animations in glTF 2.0.

# Example
An example is shown in example.py. The execution of this file with `python example.py`, will generate an animated rectangle.