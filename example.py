import os
import sys
import json
import numpy as np
import gltf_animation as gltfdriver

def rect(w=1.0, h=1.0):
    vertex_data = np.zeros(4, dtype = [
        ("position", np.float32, 3),
    ])
    
    w_half, h_half = w/2.0, h/2.0
    
    vertex_data["position"] = [(-w_half, -h_half, 0), (-w_half, +h_half, 0), (+w_half, -h_half, 0), (+w_half, +h_half, 0)]

    index_data = np.array([0,1,2,3,2,1], dtype=np.uint16)
    
    return vertex_data, index_data

def displacements(states, dw=1, dh=1):
    displacement_array = []
    counter = 0

    w_half, h_half = dw/2.0, dh/2.0

    for state in range(states):
            zeros = np.zeros(4, dtype=[
                ("position", np.float32, 3),
            ])
            displacement_array.append(zeros)
    
    for state in range(states):
        if counter == 0:
            displacement_array[counter]["position"] = [(0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0)]
        else:
             displacement_array[counter]["position"] = [(-w_half*counter, -h_half*counter, 0), (-w_half*counter, +h_half*counter, 0), (+w_half*counter, -h_half*counter, 0), (+w_half*counter, +h_half*counter, 0)]
        counter += 1
    return displacement_array


def main():
    states = 2
    vertex_data, index_data = rect()
    weight_data = np.eye(states, dtype=np.float32).ravel()
    displacement_array = displacements(states)
    animation_data = np.array(range(0, states)).astype(np.float32)

    gltf_path = "rect.gltf"
    bin_path = "rect.bin"

    gltf = gltfdriver.GLTFDriver()

    document, buffers = gltf.numpy_to_gltf(index_data, vertex_data, animation_data, displacement_array, weight_data, gltf_path, bin_path)
    gltf.save(gltf_path, bin_path, document, buffers)


if __name__ == "__main__":
    main()