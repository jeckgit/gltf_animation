import enum
import collections

import numpy as np
import os
import sys
import json


# https://github.com/KhronosGroup/glTF/tree/master/specification/2.0

class Uniform(enum.Enum):
    LOCAL = "LOCAL"
    MODEL = "MODEL"
    VIEW = "VIEW"
    PROJECTION = "PROJECTION"


# Attribute Semantics:
# POSITION, NORMAL, TEXCOORD_0, TEXCOORD_1, COLOR, JOINT, WEIGHT

class Attribute(enum.Enum):
    POSITION = "POSITION"
    NORMAL = "NORMAL"
    TEXCOORD = "TEXCOORD"
    TEXCOORD_0 = "TEXCOORD_0"
    TEXCOORD_1 = "TEXCOORD_1"
    COLOR_0 = "COLOR_0"
    JOINTS_0 = "JOINTS_0"
    WEIGHTS_0 = "WEIGHTS_0"

    @staticmethod
    def custom(value):
        return collections.namedtuple("Attribute", "value")(value)


# Accessor Types:
# SCALAR, VEC2, VEC3, VEC4, MAT2, MAT3, MAT4

class AccessorType(enum.Enum):
    SCALAR = "SCALAR"
    VEC2 = "VEC2"
    VEC3 = "VEC3"
    VEC4 = "VEC4"
    MAT2 = "MAT2"
    MAT3 = "MAT3"
    MAT4 = "MAT4"


# Component Types:
# SCALAR, VEC2, VEC3, VEC4, MAT2, MAT3, MAT4

class ComponentType(enum.Enum):
    BYTE = 5120
    UNSIGNED_BYTE = 5121
    SHORT = 5122
    UNSIGNED_SHORT = 5123
    INT = 5124
    UNSIGNED_INT = 5125
    FLOAT = 5126

    @staticmethod
    def custom(value):
        return collections.namedtuple("ComponentType", "value")(value)


class BufferTarget(enum.Enum):
    ARRAY_BUFFER = 34962
    ELEMENT_ARRAY_BUFFER = 34963


class PrimitiveMode(enum.Enum):
    POINTS = 0
    LINES = 1
    LINE_LOOP = 2
    LINE_STRIP = 3
    TRIANGLES = 4
    TRIANGLE_STRIP = 5
    TRIANGLE_FAN = 6


def askey(name):
    return name.replace(" ", "_").lower()


class Object(object):
    def __init__(self, *args, **kwargs):
        self.name = kwargs.get("name")
        self.extensions = kwargs.get("extensions")
        self.extras = kwargs.get("extras")

    def togltf(self):
        result = {}
        if self.name:
            result["name"] = self.name
        if self.extensions:
            result["extensions"] = self.extensions
        if self.extras:
            result["extras"] = self.extras
        return result


class Document(object):
    @classmethod
    def from_mesh(cls, mesh, material=None):
        materials = [] if material is None else [material]
        meshes = [] if mesh is None else [mesh]

        nodes = [Node(name="Default Node", mesh=mesh) for mesh in meshes]
        scene = Scene(name="Default Scene", nodes=nodes)
        scenes = [scene]

        return cls(materials=materials, meshes=meshes, nodes=nodes, scenes=scenes, scene=scene)

    @classmethod
    def from_meshes(cls, meshes, materials=None):
        meshes = [] if meshes is None else meshes
        materials = [] if materials is None else materials

        nodes = [Node(name="Default Node", mesh=mesh) for mesh in meshes]
        scene = Scene(name="Default Scene", nodes=nodes)
        scenes = [scene]

        return cls(materials=materials, meshes=meshes, nodes=nodes, scenes=scenes, scene=scene)

    def __init__(self, *args, **kwargs):
        self.asset = {"version": "2.0"}
        self.accessors = []
        self.animations = []
        self.buffers = []
        self.buffer_views = []
        self.cameras = []
        self.images = []
        self.materials = []
        self.meshes = []
        self.nodes = []
        self.samplers = []
        self.scenes = []
        self.scene = kwargs.get('scene', None)

        self.add_accessors(kwargs.get('accessors', []))
        self.add_animations(kwargs.get('animations', []))
        self.add_buffers(kwargs.get('buffers', []))
        self.add_buffer_views(kwargs.get('buffer_views', []))
        self.add_cameras(kwargs.get('cameras', []))
        self.add_images(kwargs.get('images', []))
        self.add_materials(kwargs.get('materials', []))
        self.add_meshes(kwargs.get('meshes', []))
        self.add_nodes(kwargs.get('nodes', []))
        self.add_samplers(kwargs.get('samplers', []))
        self.add_scenes(kwargs.get('scenes', []))

    def add_accessor(self, value):
        value.key = len(self.accessors)
        self.accessors.insert(value.key, value)

    def add_animation(self, value):
        value.key = len(self.animations)
        self.animations.insert(value.key, value)

    def add_buffer(self, value):
        value.key = len(self.buffers)
        self.buffers.insert(value.key, value)

    def add_buffer_view(self, value):
        value.key = len(self.buffer_views)
        self.buffer_views.insert(value.key, value)

    def add_camera(self, value):
        value.key = len(self.cameras)
        self.cameras.insert(value.key, value)

    def add_image(self, value):
        value.key = len(self.images)
        self.images.insert(value.key, value)

    def add_material(self, value):
        value.key = len(self.materials)
        self.materials.insert(value.key, value)

    def add_mesh(self, value):
        value.key = len(self.meshes)
        self.meshes.insert(value.key, value)

    def add_node(self, value):
        value.key = len(self.nodes)
        self.nodes.insert(value.key, value)

    def add_sampler(self, value):
        value.key = len(self.samplers)
        self.samplers.insert(value.key, value)

    def add_scene(self, value):
        value.key = len(self.scenes)
        self.scenes.insert(value.key, value)

    def add_accessors(self, values):
        for value in values:
            self.add_accessor(value)

    def add_animations(self, values):
        for value in values:
            self.add_animation(value)

    def add_buffers(self, values):
        for value in values:
            self.add_buffer(value)

    def add_buffer_views(self, values):
        for value in values:
            self.add_buffer_view(value)

    def add_cameras(self, values):
        for value in values:
            self.add_camera(value)

    def add_images(self, values):
        for value in values:
            self.add_image(value)

    def add_materials(self, values):
        for value in values:
            self.add_material(value)

    def add_meshes(self, values):
        for value in values:
            self.add_mesh(value)

    def add_nodes(self, values):
        for value in values:
            self.add_node(value)

    def add_samplers(self, values):
        for value in values:
            self.add_sampler(value)

    def add_scenes(self, values):
        for value in values:
            self.add_scene(value)

    def togltf(self):
        result = {}
        result["asset"] = self.asset
        if self.buffers:
            result["buffers"] = [buffer.togltf() for buffer in self.buffers]
        if self.buffer_views:
            result["bufferViews"] = [buffer_view.togltf() for buffer_view in self.buffer_views]
        if self.accessors:
            result["accessors"] = [accessor.togltf() for accessor in self.accessors]
        if self.animations:
            result["animations"] = [animation.togltf() for animation in self.animations]
        if self.cameras:
            result["cameras"] = [camera.togltf() for camera in self.cameras]
        if self.images:
            result["images"] = [image.togltf() for image in self.images]
        if self.materials:
            result["materials"] = [material.togltf() for material in self.materials]
        if self.meshes:
            result["meshes"] = [mesh.togltf() for mesh in self.meshes]
        if self.nodes:
            result["nodes"] = [node.togltf() for node in self.nodes]
        if self.samplers:
            result["samplers"] = [sampler.togltf() for sampler in self.samplers]
        if self.scenes:
            result["scenes"] = [scene.togltf() for scene in self.scenes]
        if self.scene:
            result["scene"] = self.scene.key
        return result


class Scene(Object):
    def __init__(self, *args, **kwargs):
        super(Scene, self).__init__(*args, **kwargs)
        self.key = -1
        self.nodes = kwargs.get('nodes', [])

    def togltf(self):
        result = super(Scene, self).togltf()
        result["nodes"] = [node.key for node in self.nodes]
        return result


class Node(Object):
    def __init__(self, *args, **kwargs):
        super(Node, self).__init__(*args, **kwargs)
        self.key = -1
        self.camera = kwargs.get('camera')
        self.children = kwargs.get('children', [])
        self.matrix = kwargs.get('matrix')
        self.mesh = kwargs.get('mesh')
        self.rotation = kwargs.get('rotation')
        self.scale = kwargs.get('scale')
        self.translation = kwargs.get('translation')
        self.weights = kwargs.get('weights')

    def togltf(self):
        result = super(Node, self).togltf()
        if self.children:
            result["children"] = [child.key for child in self.children]
        if self.camera:
            result["camera"] = self.camera.key
        if self.mesh:
            result["mesh"] = self.mesh.key
        if self.matrix:
            result["matrix"] = self.matrix
        if self.rotation:
            result["rotation"] = self.rotation
        if self.scale:
            result["scale"] = self.scale
        if self.translation:
            result["translation"] = self.translation
        if self.weights:
            result["weights"] = self.weights
        return result


class Accessor(Object):
    def __init__(self, bufferView, byteOffset=None, count=0, type=AccessorType.SCALAR,
                 componentType=ComponentType.FLOAT, *args, **kwargs):
        super(Accessor, self).__init__(*args, **kwargs)
        self.key = -1
        self.bufferView = bufferView
        self.byteOffset = byteOffset
        self.componentType = componentType
        self.normalized = False
        self.count = count
        self.type = type
        self.max = None
        self.min = None

    def togltf(self):
        result = super(Accessor, self).togltf()
        result["bufferView"] = self.bufferView.key
        result["componentType"] = self.componentType.value
        result["count"] = self.count
        result["type"] = self.type.value
        if self.bufferView:
            result["bufferView"] = self.bufferView.key
        if self.byteOffset is not None:
            result["byteOffset"] = self.byteOffset
        if self.max:
            result["max"] = self.max
        if self.min:
            result["min"] = self.min
        return result


class BufferView(Object):
    def __init__(self, buffer, byteOffset=None, byteLength=0, byteStride=None, target=BufferTarget.ARRAY_BUFFER, *args,
                 **kwargs):
        super(BufferView, self).__init__(*args, **kwargs)
        self.key = -1
        self.buffer = buffer
        self.byteOffset = byteOffset
        self.byteLength = byteLength
        self.byteStride = byteStride
        self.target = target

    def togltf(self):
        result = super(BufferView, self).togltf()
        result["buffer"] = self.buffer.key
        result["byteLength"] = self.byteLength
        if self.byteOffset is not None:
            result["byteOffset"] = self.byteOffset
        if self.byteStride is not None:
            result["byteStride"] = self.byteStride
        if self.target:
            result["target"] = self.target.value
        return result


class Buffer(Object):
    def __init__(self, byteLength=0, uri=None, *args, **kwargs):
        super(Buffer, self).__init__(*args, **kwargs)
        self.key = -1
        self.byteLength = byteLength
        self.uri = uri

    def togltf(self):
        result = super(Buffer, self).togltf()
        result["byteLength"] = self.byteLength
        if self.uri:
            result["uri"] = self.uri
        return result


class Target(Object):
    def __init__(self, attributes):
        # self.key = key
        self.attributes = attributes

    def togltf(self):
        result = {
            key.value: attribute.key for key, attribute in self.attributes.items()
        }
        return result


class Primitive(object):
    def __init__(self, attributes, indices, material, targets, mode=PrimitiveMode.TRIANGLES, **kwargs):
        self.attributes = attributes
        self.indices = indices
        self.material = material
        self.mode = mode
        self.targets = [] if targets is None else targets
        self.extensions = kwargs.get("extensions")
        self.extras = kwargs.get("extras")

    def togltf(self):
        result = {
            "attributes": {key.value: attribute.key for key, attribute in self.attributes.items()},
        }
        if self.indices:
            result["indices"] = self.indices.key
        if self.material:
            result["material"] = self.material.key
        if self.mode:
            result["mode"] = self.mode.value
        if self.targets:
            result["targets"] = [target.togltf() for target in self.targets]
        if self.extensions:
            result["extensions"] = self.extensions
        if self.extras:
            result["extras"] = self.extras
        return result


class Mesh(Object):
    def __init__(self, primitives, weights=None, *args, **kwargs):
        super(Mesh, self).__init__(*args, **kwargs)
        self.key = -1
        self.primitives = [] if primitives is None else primitives
        self.weights = [] if weights is None else weights

    def togltf(self):
        result = super(Mesh, self).togltf()
        result["primitives"] = [primitive.togltf() for primitive in self.primitives]
        result["weights"] = self.weights.tolist()
        return result


class Material(Object):
    def __init__(self, *args, **kwargs):
        super(Material, self).__init__(*args, **kwargs)
        self.key = 0
        self.values = {
            "pbrMetallicRoughness": {
                "baseColorFactor": [
                    0.6400000190734865,
                    0.6400000190734865,
                    0.6400000190734865,
                    1.0
                ],
                "metallicFactor": 0.0
            },
            "name": "base Material",
            "doubleSided": True
        }
    def __setitem__(self, key, item):
        self.values[key] = item

    def __getitem__(self, key):
        return self.values[key]

    def togltf(self):
        result = self.values
        return result


class Channel(Object):
    def __init__(self, sampler):
        self.sampler = sampler
        self.target = {
            "node": 0,
            "path": "weights"
        }

    def togltf(self):
        result = {}
        result["sampler"] = self.sampler.key
        result["target"] = self.target
        return result


class Sampler(Object):
    def __init__(self, in_input, out_output, key, interpolation=None, ):
        self.key = key
        self.in_input = in_input  # animation keyframes key
        self.interpolation = "LINEAR" if interpolation is None else interpolation
        self.out_output = out_output  # output sind weights key

    def togltf(self):
        result = {}
        result["input"] = self.in_input.key
        result["output"] = self.out_output.key
        result["interpolation"] = self.interpolation
        return result


class Animation(Object):
    def __init__(self, in_input, out_output, *args, **kwargs):
        super(Animation, self).__init__(*args, **kwargs)
        self.key = -1
        # wenn es mehr Samplers werden, mit for-Schleife, keys erzeugen
        self.samplers = [Sampler(in_input, out_output, key=0)]
        # vorerst nur ein Target ("node": 0, "path": "weights") anderes ist fuer Animation uninteressant

        self.channels = [Channel(sampler) for sampler in self.samplers]

    def togltf(self):
        result = super(Animation, self).togltf()
        result["samplers"] = [sampler.togltf() for sampler in self.samplers]
        result["channels"] = [channel.togltf() for channel in self.channels]
        return result


class GLTFDriver():
    ATTRIBUTE_BY_NAME = {
        "position": Attribute.POSITION,
        "normal": Attribute.NORMAL,
        "texCoord": Attribute.TEXCOORD,
        "texCoord0": Attribute.TEXCOORD_0,
        "texCoord1": Attribute.TEXCOORD_1,
        "color": Attribute.COLOR_0,
    }
    TARGET_BY_NAME = {
        "position": Attribute.POSITION
    }

    COMPONENT_TYPE_BY_DTYPE = {
        np.int8: ComponentType.BYTE,
        np.uint8: ComponentType.UNSIGNED_BYTE,
        np.int16: ComponentType.SHORT,
        np.uint16: ComponentType.UNSIGNED_SHORT,
        np.uint32: ComponentType.UNSIGNED_INT,
        np.float32: ComponentType.FLOAT,
    }

    ACCESSOR_TYPE_BY_SHAPE = {
        (): AccessorType.SCALAR,
        (1,): AccessorType.SCALAR,
        (2,): AccessorType.VEC2,
        (3,): AccessorType.VEC3,
        (4,): AccessorType.VEC4,
        (1, 1): AccessorType.SCALAR,
        (2, 2): AccessorType.MAT2,
        (3, 3): AccessorType.MAT3,
        (4, 4): AccessorType.MAT4,
    }

    def __init__(self):
        pass

    def from_np_type(self, dtype, shape):
        componentType = self.COMPONENT_TYPE_BY_DTYPE.get(dtype.type)
        accessorType = self.ACCESSOR_TYPE_BY_SHAPE.get(shape)
        return accessorType, componentType

    def subtype(self, dtype):
        try:
            dtype, shape = dtype.subdtype
            return dtype, shape
        except TypeError:
            dtype, shape = dtype, ()
            return dtype, shape

    def byte_length(self, buffers):
        return sum(map(lambda buffer: buffer.nbytes, [item for sublist in buffers for item in sublist]))

    def generate_structured_array_accessors(self, data, buffer_views, offset=None, count=None, name=None):
        name = "{key}" if name is None else name
        count = len(data) if count is None else count
        result = {}
        for key, value in data.dtype.fields.items():
            dtype, delta = value
            dtype, shape = self.subtype(dtype)
            accessorType, componentType = self.from_np_type(dtype, shape)
            accessor = Accessor(buffer_views[key], offset, count, accessorType, componentType,
                                name=name.format(key=key))
            attribute = self.ATTRIBUTE_BY_NAME.get(key)
            if attribute == Attribute.POSITION:
                accessor.max = np.amax(data[key], axis=0).tolist()
                accessor.min = np.amin(data[key], axis=0).tolist()
            result[attribute] = accessor
        return result

    def generate_array_accessor(self, data, buffer_view, offset=None, count=None, name=None):
        count = len(data) if count is None else count
        dtype, shape = data.dtype, data.shape
        accessorType, componentType = self.from_np_type(dtype, shape[1:])
        result = Accessor(buffer_view, offset, count, accessorType, componentType, name=name)
        result.max = [np.max(data).tolist()]
        result.min = [np.min(data).tolist()]
        return result

    def generate_structured_array_buffer_views(self, data, buffer, target, offset=None, name=None):
        name = "{key}" if name is None else name
        offset = 0 if offset is None else offset
        length = data.nbytes
        stride = data.itemsize
        result = {}
        for key, value in data.dtype.fields.items():
            dtype, delta = value
            dtype, shape = self.subtype(dtype)
            buffer_view = BufferView(buffer, offset + delta, length - delta, stride, target, name=name.format(key=key))
            result[key] = buffer_view
        return result

    def generate_array_buffer_view(self, data, buffer, target, offset=None, name=None):
        offset = 0 if offset is None else offset
        length = data.nbytes
        stride = None
        result = BufferView(buffer, offset, length, stride, target, name=name)
        return result

    def numpy_to_gltf(self, index_data, vertex_data, animation_data, displacement_data, weight_data, gltf_path,
                      bin_path):

        mesh = Mesh([], name="Default Mesh")
        document = Document.from_mesh(mesh)

        buffers = [index_data, vertex_data, animation_data, displacement_data, weight_data]

        buffer = Buffer(self.byte_length(buffers), uri=os.path.relpath(bin_path, os.path.dirname(gltf_path)),
                        name="Default Buffer")

        document.add_buffer(buffer)

        # bufferViews
        offset = 0
        index_buffer_view = self.generate_array_buffer_view(index_data, buffer, BufferTarget.ELEMENT_ARRAY_BUFFER,
                                                            offset=offset, name="Index Buffer View")
        offset += index_data.nbytes

        vertex_buffer_views = self.generate_structured_array_buffer_views(vertex_data, buffer,
                                                                          BufferTarget.ARRAY_BUFFER, offset=offset,
                                                                          name="{key} Buffer View")
        offset += vertex_data.nbytes

        animation_buffer_view = self.generate_array_buffer_view(animation_data, buffer,target=None, offset=offset,
                                                                name="Animation Buffer View")
        offset += animation_data.nbytes

        # erstellt fuer jeden displacement df ein Array
        displacement_buffer_views_ = []
        for disp in displacement_data:
            displacement_buffer_views = self.generate_structured_array_buffer_views(disp, buffer,
                                                                                    BufferTarget.ARRAY_BUFFER,
                                                                                    offset=offset,
                                                                                    name="{key} Buffer View")
            displacement_buffer_views_.append(displacement_buffer_views)
            offset += disp.nbytes
        weight_buffer_view = self.generate_array_buffer_view(weight_data, buffer, target=None,
                                                             offset=offset, name="Weight Buffer")
        offset += weight_data.nbytes

        # Accessors
        index_accessor = self.generate_array_accessor(index_data, index_buffer_view, name="Index Accessor")
        vertex_accessors = self.generate_structured_array_accessors(vertex_data, vertex_buffer_views,
                                                                    name="{key} Accessor")
        # animation accessor = keyframes
        animation_accessor = self.generate_array_accessor(animation_data, animation_buffer_view,
                                                          name="Animation Accessor")
        # erstellt fuer jeden displacement df ein accessor
        displacement_accessors_ = []
        for disp, disp_view in zip(displacement_data, displacement_buffer_views_):
            displacement_accessors = self.generate_structured_array_accessors(disp, disp_view,
                                                                              name="Displacement Accessor")

            displacement_accessors_.append(displacement_accessors)

        weight_accessor = self.generate_array_accessor(weight_data, weight_buffer_view, name="Weight Accessor")

        animations = [Animation(in_input=animation_accessor, out_output=weight_accessor)]

        targets = []
        for displacement_accessors in displacement_accessors_:
            target = Target(displacement_accessors)
            targets.append(target)
        materials = [Material()]
        primitive = Primitive(vertex_accessors, index_accessor, materials[0], targets, PrimitiveMode.TRIANGLES)

        weights = np.zeros(len(displacement_data)).astype(np.uint8)

        document.add_buffer_view(index_buffer_view)
        document.add_buffer_views(vertex_buffer_views.values())
        document.add_buffer_view(animation_buffer_view)
        for displacement_buffer_views in displacement_buffer_views_:
            document.add_buffer_views(displacement_buffer_views.values())
        document.add_buffer_view(weight_buffer_view)

        document.add_accessor(index_accessor)
        document.add_accessors(vertex_accessors.values())
        document.add_accessor(animation_accessor)
        for displacement_accessors in displacement_accessors_:
            document.add_accessors(displacement_accessors.values())
        document.add_accessor(weight_accessor)



        mesh.primitives.append(primitive)
        mesh.weights = weights
        document.animations = animations
        document.materials = materials

        return document, buffers

    def save(self, gltf_path, bin_path, document, buffers):
        data = document.togltf()
        with open(gltf_path, 'w') as f:
            json.dump(data, f, indent=2)

        with open(bin_path, 'wb') as f:
            for buffer in buffers:
                if isinstance(buffer, list):
                    for el in buffer:
                        f.write(el.tobytes())
                else:
                    f.write(buffer.tobytes())
